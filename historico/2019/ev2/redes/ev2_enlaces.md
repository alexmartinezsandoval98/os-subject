# Enlaces

## SSH

### Funcionamiento

Abre una terminal remota

### Instalación

sudo apt install openssh-server

### Configuración


### Uso

```bash
ssh [user@]IP

ssh 172.19.70.9

# Actualizar un equipo remoto.
ssh 172.19.70.9 "apt update"

# Conectar mi servidor de ventanas (X)
# con el equipo remoto.

ssh -X user@172.19.70.8

```

## Copia de archivos

### scp

```bash
scp user1@ip_origen:/ruta/fichero_a_copiar user2@ip_destino:/ruta_destino
scp Descargas/algo.iso smyr01@172.19.70.17:/home/smyr01/
```

### netcat

```bash
# Equipo que recibe
nc -l 8888 | pigz -d | tar xvf -

# Equipo que envía
tar cf - Descargas/ | pv -s `du -sb Descargas/ | cut -f1 ` | pigz | nc -q0 172.19.70.2
```
