#!/bin/bash
# Prints a square on a terminal
# Use it on a terminal unless you enable framebuffer under X


# User config
# For a rough approx try: cat /sys/class/graphics/fb0/virtual_size
screen_width=1024

# End of user config

PROG_NAME=$0
PROG_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" >/dev/null && pwd  )"
. "$PROG_DIR/../share/suite.sh"

fbdev=/dev/fb0
ifile=$(find "$PROG_DIR/cotd_images" -type f | shuf -n 1)

bpp=4

width=$(identify -format "%w" "$ifile")> /dev/null
height=$(identify -format "%h" "$ifile")> /dev/null

# Default parameter values
xorfile=$fbdev
pos_fixed=(false false)
fixed_width=300

# getopt parameter setup
width=${fixed_width:-$width}

# usage - Displays help
function usage {
    cat << EOH

    $0 - Cartoon Of The Day

         Makes an overlay graphic on terminal (not pseudo terminal) screen
         using frame buffer.

    Note: Maybe you need to be member of 'video' group to gain access to /dev/fb0
          Try: 'cat /dev/urandom > /dev/fb0' to check whether you have access.
          You also need to edit this script and get screen width resolution. A rough approximation may be got by:
          cat /sys/class/graphics/fb0/virtual_size
          You might need a bigger ( 10px? ) value due to overscan.

    Options:
        -W:         Resize to fit in width.
        -H:         Resize to fit in height.
        -b:         Blend / Xor image with background. Slows drawing.
        -r:         Emulates old monitors like in a raster screen
        -x, -y:     Sets overlay position.
        -f:         Specifies an input file. Otherwise it chooses
                    a random image.
        -h:         Shows help.


EOH
}

# Process arguments

while getopts ":W:H:brx:y:f:h" opt; do
    case $opt in
        W)
            fixed_width=$OPTARG
            ;;
        H)
            fixed_height=$OPTARG
            ;;
        b)
            format $CYAN "Not yet implemented. Sorry!"
            exit 0
            ;;
        r)
            xorfile=/dev/null
            ;;
        x)
            x0=$OPTARG;
            pos_fixed[0]=true
            ;;
        y)
            pos_fixed[1]=true
            y0=$OPTARG
            ;;
        f)
            ifile=$OPTARG
            ;;
        h)
            ansset $GREEN; usage; ansreset
            exit 0
            ;;
        \?)
            ansset $GREEN; usage; ansreset
            format $RED "Invalid option: -$OPTARG" >&2
            echo
            exit 1
            ;;
        :)
            ansset $GREEN; usage; ansreset
            format $RED "Option -$OPTARG requires an argument" >&2
            echo
            exit 1
            ;;

    esac
done

function resize_text {
    if [ -v fixed_width -o -v fixed_height ]; then
        echo -n "-resize "
        if [ -v fixed_width ]; then
            echo -n "${fixed_width}"
        fi
        echo -n "x"
        if [ -v fixed_height ]; then
            echo -n "${fixed_height}"
        fi
    fi
}

# Calculate final width and height
resolution=$(convert $ifile $(resize_text) png:-| identify - | sed  -n 's/.*\b\([0-9]*\)x\([0-9]*\)\b.*/\1x\2/p')

width=$(cat <<< "$resolution" | cut -dx -f1)
height=$(cat <<< "$resolution" | cut -dx -f2)

[[ ${pos_fixed[0]} = false ]] && x0=$(( $screen_width - $width ))
[[ ${pos_fixed[1]} = false ]] && y0=0

line=0
convert $ifile -separate -swap 0,2 -combine $( resize_text ) -separate swap 1,4 -combine rgba:- 2> /dev/null | \
    while [ "$line" -lt "$height" ]; do
        if [[ $((line%2)) -eq 0 ]]; then
            dd   count=$width\
                bs=$bpp\
                seek=$(( ($((line++)) + $y0) * $screen_width + $x0 ))\
                of=$fbdev \
                &> /dev/null
        else
            dd   count=$width\
                bs=$bpp\
                seek=$(( ($((line++)) + $y0) * $screen_width + $x0 ))\
                of=$xorfile \
                &> /dev/null
        fi
    done
