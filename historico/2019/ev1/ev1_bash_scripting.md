# BASH Scripting

Terminal, Shell

Bourne Again SHell

```
     +------------+
     |  Usuario   |
     +--+------+--+
        |      |
+------ V --+  V
| +-------+ |  +-----------+
| | SHELL | |  | Programas |
| +---+---+ |  +-+------+--+
|     |     +----|-----)|(----+
|     V  S.O     V      |     |
| +--------------------)|(+   |  Máquina Virtual: hardware virtual
| |     Kernel    HW   DX |   |  DirectX: Posibilidad de acceder al Hard
| +-+-------+------+---)|(+   |
+---|-------|------|----|-----+
    V       V   driVer  V        Driver: Trozo de código hecho por el
  +---+   +---+  +---+ +-+               fabricante para adaptarse al
  | HW|   | HW|  | HW| | |               dispositivo virtual.
  +---+   +---+  +---+ +-+
```

`#!`: shebang => Con qué programa hay que interpretar y ejecutar el siguiente texto.  
`#`: Comentario.  
`$1`: Argumento 1  
`dirname <texto>`: Extrae el nombre del directorio del texto.  
`basename <texto>`: Extrae el nombre del fichero del texto.  
`${var:-valor}`: Si var no contiene nada va a valer: valor.  
`${VARIABLE#.*}` : Borrar por la izquierda del `.*`
`algo.png.txt => png.txt`  
`${VARIABLE##.*}`: Borrar por la izquierda lo más posible del `.*`
`algo.png.txt => txt`  
`${VARIABLE%.*}`: Borrar por la derecha.  

## Ejecución
`<comando> &`: Ejecutar en segundo plano.  
`Ctl z`: Pasar a segundo plano.  
`jobs`: Tareas detenidas.  
`fg <num>`: Pasar a primer plano un proceso.  

`()`: Subshell.  
`(())`: Expansión aritmética.  

`$()`: Igual que la comilla haci atrás. Ejecuta y quédate con el resultado.  
`<()`:  


