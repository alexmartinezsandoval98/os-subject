# CONEXIONES FISICAS

## Palabras Clave:

loopback: Interfaz de bucle.  
127.0.0.1 = localhost.  


## Proceso de Conexión

![Conexión de Entrada via NAT](../img/NAT.png)


## Comandos

### De Testeo

`ifconfig`: Mirar nuestra dirección IP.  
`ping`: Comprueba la conexión.  
Debemos de comprobar:

1. Conexión local
1. A la puerta de enlace
1. A un equipo externo
1. La resolución de nombres de dominio.




