# CONEXIONES LÓGICAS

URL: Universal Resource Locator - Localizador Universal de Recursos.  
URN: Universal Resource Name  
URI: Universal Resource Identifier  

Esquema de una URL

    http://maquina.com/directorio/fichero.txt

En términos coloquiales:

    protocolo://máquina/directorio/directorio/fichero

Más formalmente:

    esquema://[usuario[:password]@anfitrion[:puerto]/ruta?consulta#fragmento

La consulta es una manera de enviar información al servidor.  
El fragmento (ancla/anchor) indica qué parte del documento queremos ver.  



PROTOCOLOS.

Los más conocidos:

1. http: Hypertext Transfer Protocol.
1. https: HTTP sobre Secure Sockets Layer (SSL).
1. ftp: File Transfer Protocol.
1. mailto: Direcciones de correo electrónico.
1. ldap: Lightweight Directory Access Protocol. Directorio Activo. Bases de datos de usuarios para inicios de sesión principalmente.
1. file: recursos disponibles en local.
1. gopher: en desuso
1. telnet: Conexiones punto a punto
1. data: Insertar datos en los documentos.


## Proceso de conexión mediante el navegador.

### Conceptos
DN: Domain Name. Nombre de Dominio.  
DNS: Domain Name Server. Servidor de Nombres de Dominio.  
Resolución de Nombre de Dominio.

DNS Leaks
Servicios a través del DNS

/etc/hosts


Herramientas para comprobar la conexión

ping
dig
whois

Servidores
BIND9
Apache
mail

BIND
