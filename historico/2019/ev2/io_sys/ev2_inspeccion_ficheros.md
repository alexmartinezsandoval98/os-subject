# Inspección de Ficheros

Poder poner información en pantalla es la condición necesaria
para poder empezar a filtrar.

`cat`: Muestra el contenido de uno o varios ficheros
uno a continuación del otro.  
`tail`: Ver el final del fichero.  
    `tail -20 <fichero>`: Últimas 20 lineas del fichero.  
    `tail -f <fichero>`: Ver sin cerrar del fichero.  
`head`: Mirar el principio de un fichero.  
`od`: _octal dump_.  Volcado de octetos.  
    `od -tx1 fichero`  
    `od -tc fichero`  
    `od -tcx1 fichero`  

`more`: Muestra una pantalla y para (espacio en intro).  
`less`: _less is more than more_. Como more, pero carga en RAM.  


## Ficheros Importantes

1. `/var/log/syslog`
1. `/var/log/authlog`
1. `/var/log/apache2/*`
1. `/dev/null`

