# Startup Files

Hay ejemplos en `/usr/share/doc/bash/examples/startup-files`.

## .profile

Es para cualquier terminal genérica.  
No se ejecuta si existe `.bash_profile` o `.bash_login`.

## .bashrc

Para shells que no usan login.  
Se pueden poner cosas específicas de bash:

- alias
- prompt

A veces se definen las variables de entorno aquí.

## .bash_profile

Para definir las variables de entorno.  
Generalmente incluye:

```bash
. ~/.profile
. ~/.bashrc
```
## .bash_login

Para las terminales que usan login.



