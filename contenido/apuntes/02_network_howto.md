# Redes

## Gestor
Fichero: /etc/init.d/network-manager 
Es un script de inicio que arranca el gestor de conexiones:
/usr/sbin/network-manager
La configuracion está en:
/etc/network/interfaces

## Manual

### Adaptador
ifdown eth0
ifup eth0

### Configuración
ifconfig eth0 up 192.168.29.39 netmask 255.255.255.0
route add default gw 192.168.29.1

FQDN = Fully Qualified Domain Name
hostname profesor.salesianosdosa.com
/etc/hosts      : Lista de IPs y nombres
/etc/hostname   : Nombre de mi maquina (se cambia con el comando hostname)

traceroute # registra los sitios por los que pasan los paquetes.
arp

openVPN
## Wifi
iwlist wlan0 scan
iwconfig wlan0 essid MOVISTAR_7C1 channel 1 mode manager key s:N1mP7mHNw

